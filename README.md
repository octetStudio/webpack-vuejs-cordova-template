# Webpack + VueJS + Cordova bootstrap

**Attention : don't touch to www folder. ** Work in src folder, and in index.html at root.

## Build Setup

### install dependencies
``` npm install ```

### vue.js only build
``` npm run build ```

### cordova full build (include vue.js build)
```cordova build browser && cordova run browser```
