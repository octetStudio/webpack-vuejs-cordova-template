import Vue from 'vue'
import App from './App'
import router from './router'

console.log('Start 1')

// conf
Vue.config.productionTip = false

// cordova + vue init
var cordova = {

  init: function () {
    console.log('Init...')
    document.addEventListener('deviceready', this.onDeviceReady.bind(this), false)
  },

  initVueJs: function () {
    console.log('initVueJs...')
    /* eslint-disable no-new */
    new Vue({
      el: '#app',
      router,
      template: '<App/>',
      components: { App }
    })
  },

  onDeviceReady: function () {
    console.log('onDeviceReady...')
    this.initVueJs()
    var element = document.getElementById('helloCordova')
    element.innerHTML = 'Hello, this is Cordova.'
    console.log('Hello, this is Cordova.')
  }
}

cordova.init()
console.log('Start 2')
